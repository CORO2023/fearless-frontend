import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './locationForm';
import ConferenceForm from './conferenceForm';
import AttendConference from './AttendConference';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <AttendConference/>
      {/* <conferenceForm/> */}
      {/* <locationForm/> */}
      {/* <attendeesList attendees={props.attendees}/> */}
    </div>
    </>
  );
}

export default App;
